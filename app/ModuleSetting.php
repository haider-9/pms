<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleSetting extends Model
{

    protected $guarded = ['id'];

    public static function checkModule($moduleName) {
        $user = auth()->user();
        $isAdmin = User::isAdmin($user->id);
        $isClient = User::isClient($user->id);
        $isEmployee = User::isEmployee($user->id);

        $module = ModuleSetting::where('module_name', $moduleName);

        if($isAdmin){ $module =  $module->where('type', 'admin'); }
        elseif($isClient){ $module =  $module->where('type', 'client'); }
        elseif($isEmployee){ $module = $module->where('type', 'employee'); }

        $module = $module->first();
        if($module){
            if($module->status == 'active'){
                return true;
            }
        }

        return false;
    }
}
